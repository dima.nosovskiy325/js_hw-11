/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

/* 

Теория

1) Механизм с помощью которого можно взаимодействовать с пользователем или реагировать на действия в браузере, используются чтобы сделать страницу динамической и интерактивной
2) mouseover / mouseout / mouseup / mousedown / click / dblclick / mousemove / mouseleave
3) Это событие позволяет видеть что пользователь хочет вызвать контекстное меню и можно например отобразить собственное кастомное контекст меню.

*/

// Практика 

// 1 

const searchingButton = document.querySelector('#btn-click');
const searchingSection = document.querySelector('#content');

searchingButton.addEventListener('click', () => {
    const paragraph = document.createElement('p');
    paragraph.textContent = 'New Paragraph';

    searchingSection.prepend(paragraph)
})

// 2 


const newButton = document.createElement('button');

newButton.setAttribute('id', 'btn-input-create');

newButton.textContent = 'click me';

newButton.style.cssText = 'display: block; margin: 0 auto;'

newButton.addEventListener('click', () => {
    const input = document.createElement('input');

    for(let i = 0; i < 2; i++) {
        input.setAttribute('type', 'text');
        input.setAttribute('placeholder', 'Введите вопрос?');
        input.setAttribute('name', 'dmytro');
    }
    input.style.cssText = 'display: block; margin: 0 auto;'
    newButton.after(input);
})

searchingSection.insertAdjacentElement('afterend', newButton)


